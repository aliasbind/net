#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>

#define MSG	"This is a message."

int main(int argc, char *argv[])
{
	int sockfd[2], err;
	pid_t pid;
	size_t sz;

	char buf[64];

	err = socketpair(AF_LOCAL, SOCK_STREAM, 0, sockfd);
	if (err < 0) {
		perror("socketpair");
		return EXIT_FAILURE;
	}

	pid = fork();
	if (pid < 0) {
		perror("fork");
		return EXIT_FAILURE;
	}

	if (pid == 0) {
		strcpy(&buf[0], MSG);
		printf("Sending \"%s\"\n", buf);
		sz = send(sockfd[0], MSG, strlen(MSG) + 1, 0);
		if (sz < 0) {
			perror("send");
			return EXIT_FAILURE;
		}
		printf("Sent %ld out of %ld bytes.\n", sz, strlen(MSG) + 1);

		sz = recv(sockfd[0], &buf[0], sizeof(buf), 0);
		if (sz < 0) {
			perror("send");
			return EXIT_FAILURE;
		}
		printf("Received back %ld out of %ld bytes.\n", sz, strlen(MSG) + 1);
		printf("Result: \"%s\"\n", buf);
		return EXIT_SUCCESS;
	} else {
		sz = recv(sockfd[1], &buf[0], sizeof(buf), 0);
		if (sz < 0) {
			perror("send");
			return EXIT_FAILURE;
		}
		printf("Received %ld bytes. Sending them back...\n", sz);

		sz = send(sockfd[1], &buf[0], sz, 0);
		if (sz < 0) {
			perror("send");
			return EXIT_FAILURE;
		}
		printf("Sent back %ld bytes.\n", sz);

		close(sockfd[0]);
		close(sockfd[1]);
		return EXIT_SUCCESS;
	}
}
