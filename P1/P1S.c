#define PORT	"3490"
#define BACKLOG	50

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common.h"
#include "frame_api.h"

static int32_t server_process(char *buf, int buf_len, int *status)
{
	int i, first;

	int clear[256];
	int prio_ops[256], prio_ops_len;

	int32_t n1, n2, rez;
	char op;

#ifdef CONFIG_DEBUG
	printf("Processing frame:\n");
	print_frame(&buf[0], buf_len);
#endif

	memset(&clear[0], 0, sizeof(int) * 256);
	prio_ops_len = 0;

	for (i = 0; i < get_values_length(&buf[0], buf_len) - 1; i++) {
		if (get_op_near(&buf[0], buf_len, i) == MULTIPLY ||
				get_op_near(&buf[0], buf_len, i) == DIVIDE) {
			prio_ops[prio_ops_len] = i;
			prio_ops[prio_ops_len+1] = i + 1;
			prio_ops_len += 2;
		}
	}

	for (i = 0; i < prio_ops_len; i += 2) {
		n1 = get_frame_elem(&buf[0], buf_len, prio_ops[i]);
		op = get_op_near(&buf[0], buf_len, prio_ops[i]);
		n2 = get_frame_elem(&buf[0], buf_len, prio_ops[i + 1]);

		clear[prio_ops[i]] = 1;

		set_frame_elem(&buf[0], buf_len, prio_ops[i + 1],
				op_eval(n1, n2, op));
	}

	first = 1;
	for (i = 0; i < get_values_length(&buf[0], buf_len); i++) {
		if (clear[i])
			continue;

		if (first) {
			first = 0;
		} else {
			n2 = get_frame_elem(&buf[0], buf_len, i);
			set_frame_elem(&buf[0], buf_len, i,
					op_eval(n1, n2, op));
		}

		n1 = get_frame_elem(&buf[0], buf_len, i);
		op = get_op_near(&buf[0], buf_len, i);
	}

#ifdef CONFIG_DEBUG
	printf("Result:\n");
	for (i = buf_len - 4; i < buf_len; i++)
		printf("\e[1;32m%02X \e[0m", (unsigned char) buf[i]);
	printf("\n");
#endif
	memcpy(&rez, &buf[buf_len - 4], sizeof(uint32_t));

	return rez;
}

static void server_loop(int sockfd)
{
	char buf[256];

	int err, curr_fd;
	int32_t rez;
	struct sockaddr_storage client;
	pid_t child;
	socklen_t sockstore_size;

	fd_set client_set;
	FD_ZERO(&client_set);

	while (1) {
		FD_SET(sockfd, &client_set);
		err = select(sockfd + 1, &client_set, NULL, NULL, NULL);
		if (err < 0)
			handle_error("select");

		if (!FD_ISSET(sockfd, &client_set))
			continue;

		sockstore_size = sizeof(struct sockaddr_storage);
		curr_fd = accept(sockfd, (struct sockaddr *) &client,
				&sockstore_size);
		if (curr_fd < 0) {
			perror("accept");
			continue;
		}

		child = fork();
		if (child < 0) {
			perror("fork");
			continue;
		}

		if (child == 0) {
			close(sockfd);

			err = recv(curr_fd, &buf[0], 256, 0);
			if (err < 0)
				perror("recv");

			rez = server_process(&buf[0], err, NULL);

			err = send(curr_fd, &rez, sizeof(int32_t), 0);
			if (err < 0)
				perror("send");

			close(curr_fd);
			exit(EXIT_SUCCESS);
		}
		close(curr_fd);
	}
}

static int bind_sock(int sockfd, struct addrinfo *inf)
{
	int err, yes = 1;

	if (sockfd < 0) {
		perror("socket");
		return -1;
	}

	err = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
			sizeof(int));
	if (err < 0) {
		close(sockfd);
		perror("setsockopt");
		return -1;
	}

	err = bind(sockfd, inf->ai_addr, inf->ai_addrlen);
	if (err < 0) {
		close(sockfd);
		perror("bind");
		return -1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int sockfd, err;
	struct addrinfo hints;
	struct addrinfo *res, *p;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	err = getaddrinfo(NULL, PORT, &hints, &res);
	if (err < 0) {
		printf("%s\n", gai_strerror(err));
		return EXIT_FAILURE;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (!bind_sock(sockfd, p))
			break;
	}

	if (p == NULL) {
		printf("could not bind to any socket\n");
		return EXIT_FAILURE;
	}

	freeaddrinfo(res);

	err = listen(sockfd, BACKLOG);
	if (err < 0)
		handle_error("listen");

	server_loop(sockfd);

	return EXIT_SUCCESS;
}
