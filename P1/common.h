#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0);

enum {
	ADD,
	SUBSTRACT,
	MULTIPLY,
	DIVIDE,
};

void print_frame(char *buf, int buf_len);

#endif
