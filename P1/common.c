#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

void print_frame(char *buf, int buf_len)
{
	int num_idx = 0, i;

	printf("\e[1;34m%02X \e[0m", (unsigned char) buf[0]);
	for (i = 1; i < buf_len; i++) {
		if (num_idx % sizeof(int32_t) == 0 && num_idx > 0) {
			printf("\e[1;31m%02X \e[0m", (unsigned char) buf[i]);
			num_idx = 0;
		} else {
			printf("\e[1;32m%02X \e[0m", (unsigned char) buf[i]);
			num_idx++;
		}
	}

	printf("\n");
}
