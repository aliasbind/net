#include "common.h"
#include "frame_api.h"

int32_t get_frame_elem(char *buf, int buf_len, int pos)
{
	int32_t elem;
	const int buf_pos = 1 + (sizeof(int32_t) + 1) * pos;

	if (buf_pos > buf_len)
		printf("set_frame_elem: Position outside of frame\n");

	memcpy(&elem, &buf[1 + (sizeof(int32_t) + 1) * pos], sizeof(int32_t));

	return elem;
}

void set_frame_elem(char *buf, int buf_len, int pos, int32_t elem)
{
	const int buf_pos = 1 + (sizeof(int32_t) + 1) * pos;

	if (buf_pos > buf_len)
		printf("set_frame_elem: Position outside of frame\n");

	memcpy(&buf[1 + (sizeof(int32_t) + 1) * pos], &elem, sizeof(int32_t));
}

char get_op_near(char *buf, int buf_len, int pos)
{
	return buf[1 + (sizeof(int32_t) + 1) * pos + sizeof(int32_t)];
}

int32_t op_eval(int32_t a, int32_t b, char op)
{
	switch (op) {
	case ADD:
		return a + b;
	case SUBSTRACT:
		return a - b;
	case MULTIPLY:
		return a * b;
	case DIVIDE:
		return a / b;
	}

	return 0;
}

int get_values_length(char *buf, int buf_len)
{
	int i = 1, len = 0;

	while (i < buf_len) {
		i += sizeof(int32_t) + 1;
		len++;
	}

	return len;
}
