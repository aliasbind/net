#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PORT	"3490"

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0);

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common.h"

char char_to_op(char c)
{
	switch (c) {
	case '+':
		return ADD;
	case '-':
		return SUBSTRACT;
	case '*':
		return MULTIPLY;
	case '/':
		return DIVIDE;
	}

	return 0xFF;
}

char *input_to_frame(char *buf, int buf_len, int *outbuf_len)
{
	int buf_idx = 0, read_size;
	char *outbuf;
	int outbuf_idx = 0;

	int32_t num;
	char op;

	outbuf = malloc(sizeof(char) * 256);
	if (!outbuf)
		handle_error("malloc");

	memset(&outbuf[0], 0, sizeof(char) * 256);
	outbuf[outbuf_idx++] = sizeof(int32_t);

	while (buf_idx < buf_len) {
		sscanf(&buf[buf_idx], "%d%n", &num, &read_size);
		memcpy(&outbuf[outbuf_idx], &num, sizeof(int32_t));

		outbuf_idx += sizeof(int32_t);
		buf_idx += read_size;

		if (buf_idx < buf_len) {
			sscanf(&buf[buf_idx++], "%c", &op);
			outbuf[outbuf_idx++] = char_to_op(op);
		}
	}

	*outbuf_len = outbuf_idx;
	return outbuf;
}

int main(int argc, char *argv[])
{
	char buf[256], *rez;
	int buf_len, rez_len;
	FILE *f;

	int sockfd, err;
	int32_t result;
	struct addrinfo hints;
	struct addrinfo *res, *p;

	if (argc != 2) {
		printf("Usage: %s <address>\n", argv[0]);
		return EXIT_FAILURE;
	}

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	err = getaddrinfo(argv[1], PORT, &hints, &res);
	if (err < 0) {
		printf("%s\n", gai_strerror(err));
		return EXIT_FAILURE;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (sockfd < 0) {
			perror("socket");
			continue;
		}

		err = connect(sockfd, p->ai_addr, p->ai_addrlen);
		if (err < 0) {
			perror("connect");
			close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		printf("failure to connect\n");
		return EXIT_FAILURE;
	}

	freeaddrinfo(res);

	f = fopen("file.in", "rw");
	fscanf(f, "%256[^\n]%n", &buf[0], &buf_len);

	rez = input_to_frame(&buf[0], buf_len, &rez_len);

#ifdef CONFIG_DEBUG
	printf("Sending to %s:\n", argv[1]);
	print_frame(&rez[0], rez_len);
#endif

	err = send(sockfd, &rez[0], rez_len, 0);
	if (err < 0)
		perror("send");
	free(rez);

	err = recv(sockfd, &result, sizeof(int32_t), 0);
	if (err < 0)
		perror("recv");

	printf("Received result from server: %d\n", result);
	return EXIT_SUCCESS;
}
