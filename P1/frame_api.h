#ifndef _FRAME_API_H
#define _FRAME_API_H

int32_t get_frame_elem(char *buf, int buf_len, int pos);

void set_frame_elem(char *buf, int buf_len, int pos, int32_t elem);

int32_t op_eval(int32_t a, int32_t b, char op);

char get_op_near(char *buf, int buf_len, int pos);

int get_values_length(char *buf, int buf_len);

#endif
